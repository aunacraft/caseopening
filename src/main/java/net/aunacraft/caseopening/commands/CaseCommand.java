package net.aunacraft.caseopening.commands;

import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.autocompleetion.impl.IntegerCompletionHandler;
import net.aunacraft.api.commands.autocompleetion.impl.NoCompletionHandler;
import net.aunacraft.api.commands.autocompleetion.impl.PositiveIntegerCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.caseopening.CaseOpening;
import net.aunacraft.caseopening.cases.CaseCache;
import net.aunacraft.caseopening.cases.CaseOpeningCase;
import net.aunacraft.caseopening.commands.autocompleetionhandler.ChestOpeningCaseNameAutoCompletionHandler;
import net.aunacraft.caseopening.commands.autocompleetionhandler.ExecutorAutoCompleetionHandler;
import net.aunacraft.caseopening.items.CaseOpeningItemExecutor;
import net.aunacraft.caseopening.utils.CaseOpeningItemUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Locale;
import java.util.UUID;

public class CaseCommand implements AunaCommand {


    //case edit <name>
    //case create <name>
    //case createitem <amount> <executor> [command]
    //case delete <name>
    //case list

    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("caseopening")
                .subCommand(
                        CommandBuilder.beginCommand("setblock")
                                .permission("caseopening.setblock")
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    CaseOpening.getPlugin(CaseOpening.class).setCaseOpeningBlock(aunaPlayer.toBukkitPlayer().getLocation().getBlock());
                                    aunaPlayer.sendRawMessage("§aBlock gesetzt!");
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("reload")
                                .permission("caseopening.reload")
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    CaseCache.reload();
                                    aunaPlayer.sendRawMessage("§aReloaded Cases §7(Loaded " + CaseCache.getCases().size() + " Cases)");
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("create")
                                .permission("caseopening.create")
                                .parameter(
                                        ParameterBuilder.beginParameter("name")
                                                .required()
                                                .autoCompletionHandler(new NoCompletionHandler())
                                                .build()
                                )
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    String caseName = commandContext.getParameterValue("name", String.class);
                                    if(CaseCache.exists(caseName)) {
                                        aunaPlayer.sendRawMessage("§cEine Case mit diesem namen existiert bereits!");
                                        return;
                                    }
                                    CaseOpeningCase caseOpeningCase = new CaseOpeningCase(UUID.randomUUID(), caseName);
                                    CaseCache.addCase(caseOpeningCase);
                                    aunaPlayer.sendRawMessage("§7Du hast die Case §e" + caseName + " §7erstellt!");
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("delete")
                                .permission("caseopening.delete")
                                .parameter(
                                        ParameterBuilder.beginParameter("name")
                                                .required()
                                                .autoCompletionHandler(new NoCompletionHandler())
                                                .build()
                                )
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    String caseName = commandContext.getParameterValue("name", String.class);
                                    if(!CaseCache.exists(caseName)) {
                                        aunaPlayer.sendRawMessage("§cEine Case mit diesem namen existiert nicht!");
                                        return;
                                    }
                                    CaseOpeningCase caseOpeningCase = new CaseOpeningCase(UUID.randomUUID(), caseName);
                                    CaseCache.remove(caseOpeningCase);
                                    aunaPlayer.sendRawMessage("§7Du hast die Case §e" + caseName + " §cgelöscht!");
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("edit")
                                .permission("caseopening.edit")
                                .parameter(
                                        ParameterBuilder.beginParameter("case")
                                                .required()
                                                .autoCompletionHandler(new ChestOpeningCaseNameAutoCompletionHandler())
                                                .build()
                                )
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    CaseOpeningCase caseOpeningCase = commandContext.getParameterValue("case", CaseOpeningCase.class);
                                    if(caseOpeningCase.isEditing()) {
                                        aunaPlayer.sendRawMessage("§cDiese Case wird bereits von: " + caseOpeningCase.getEditor().getName() + " bearbeitet!");
                                        return;
                                    }
                                    caseOpeningCase.edit(aunaPlayer.toBukkitPlayer());
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("createitem")
                                .permission("caseopening.createitem")
                                .parameter(
                                        ParameterBuilder.beginParameter("amount")
                                                .autoCompletionHandler(new PositiveIntegerCompletionHandler(1, 5, 10, 100))
                                                .required()
                                                .build()
                                )
                                .parameter(
                                        ParameterBuilder.beginParameter("executor")
                                                .autoCompletionHandler(new ExecutorAutoCompleetionHandler())
                                                .required()
                                                .build()
                                )
                                .parameter(
                                        ParameterBuilder.beginParameter("command")
                                                .autoCompletionHandler(new ExecutorAutoCompleetionHandler())
                                                .optional()
                                                .openEnd()
                                                .build()
                                )
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    Player player = aunaPlayer.toBukkitPlayer();
                                    ItemStack itemInHand = player.getInventory().getItemInMainHand();
                                    if(itemInHand.getType().equals(Material.AIR)) {
                                        aunaPlayer.sendRawMessage("§cBitte nehme zuerst ein Item in die Hand!");
                                    }else {
                                        String executorIdentifier = commandContext.getParameterValue("executor", String.class).toLowerCase();
                                        CaseOpeningItemExecutor executor;
                                        if(executorIdentifier.equals("console_command") || executorIdentifier.equals("player_command")) {
                                            if(!commandContext.hasParameter("command")) {
                                                aunaPlayer.sendRawMessage("§cBitte gebe auch einen Command an!");
                                                return;
                                            }else {
                                                String command = commandContext.getParameterValue("command", String.class);
                                                if(executorIdentifier.equals("console_command")) {
                                                    executor = CaseOpeningItemExecutor.consoleCommandItemExecutor(command);
                                                }else {
                                                    executor = CaseOpeningItemExecutor.playerCommandItemExecutor(command);
                                                }
                                            }
                                        }else executor = CaseOpeningItemExecutor.giveItemExecutor;

                                        int amount = commandContext.getParameterValue("amount", Integer.class);
                                        ItemStack finalStack = CaseOpeningItemUtil.createCaseItem(itemInHand, amount, executor);
                                        player.getInventory().setItemInMainHand(finalStack);
                                        aunaPlayer.sendRawMessage("§aItem erstellt!");
                                    }
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("rename")
                                .permission("caseopening.rename")
                                .parameter(
                                        ParameterBuilder.beginParameter("case")
                                                .required()
                                                .autoCompletionHandler(new ChestOpeningCaseNameAutoCompletionHandler())
                                                .build()
                                )
                                .parameter(
                                        ParameterBuilder.beginParameter("name")
                                                .required()
                                                .autoCompletionHandler(new NoCompletionHandler())
                                                .build()
                                )
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    CaseOpeningCase caseOpeningCase = commandContext.getParameterValue("case", CaseOpeningCase.class);
                                    String oldName = caseOpeningCase.getName();
                                    caseOpeningCase.setName(commandContext.getParameterValue("name", String.class));
                                    aunaPlayer.sendRawMessage("§7Du hast den Namen vom Crate &e" + oldName + " §7zu §e" + caseOpeningCase.getName()+ " §7geändert.");
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("list")
                                .permission("caseopening.list")
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    for (CaseOpeningCase caseOpeningCase : CaseCache.getCases()) {
                                        aunaPlayer.sendRawMessage("§8- §e" + caseOpeningCase.getName());
                                    }
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("getkey")
                                .permission("caseopening.getkey")
                                .parameter(
                                        ParameterBuilder.beginParameter("case")
                                                .required()
                                                .autoCompletionHandler(new ChestOpeningCaseNameAutoCompletionHandler())
                                                .build()
                                )
                                .parameter(
                                        ParameterBuilder.beginParameter("amount")
                                                .optional()
                                                .autoCompletionHandler(new PositiveIntegerCompletionHandler())
                                                .build()
                                )
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    int amount = commandContext.hasParameter("amount") ? commandContext.getParameterValue("amount", Integer.class) : 1;
                                    ItemStack keyStack = commandContext.getParameterValue("case", CaseOpeningCase.class).getKeyItem();
                                    keyStack.setAmount(amount);
                                    aunaPlayer.toBukkitPlayer().getInventory().addItem(keyStack);
                                })
                                .build()
                )
                .build();
    }
}
