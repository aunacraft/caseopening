package net.aunacraft.caseopening.commands.autocompleetionhandler;

import net.aunacraft.api.commands.autocompleetion.AutoCompletionHandler;
import net.aunacraft.caseopening.items.CaseOpeningItemExecutor;

import java.util.List;

public class ExecutorAutoCompleetionHandler implements AutoCompletionHandler<String> {

    @Override
    public List<String> getCompletions() {
        return CaseOpeningItemExecutor.getIdentifiers();
    }

    @Override
    public boolean isValid(String s) {
        return CaseOpeningItemExecutor.isValidIdentifier(s);
    }

    @Override
    public String convertString(String s) {
        return s;
    }
}
