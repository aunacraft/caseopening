package net.aunacraft.caseopening.commands.autocompleetionhandler;

import com.google.common.collect.Lists;
import net.aunacraft.api.commands.autocompleetion.AutoCompletionHandler;
import net.aunacraft.caseopening.cases.CaseCache;
import net.aunacraft.caseopening.cases.CaseOpeningCase;

import java.util.List;

public class ChestOpeningCaseNameAutoCompletionHandler implements AutoCompletionHandler<CaseOpeningCase> {

    @Override
    public List<String> getCompletions() {
        List<String> matches = Lists.newArrayList();
        CaseCache.getCases().forEach(caseOpeningCase -> matches.add(caseOpeningCase.getName()));
        return matches;
    }

    @Override
    public boolean isValid(String s) {
        return CaseCache.exists(s);
    }

    @Override
    public CaseOpeningCase convertString(String s) {
        return CaseCache.getCaseByName(s);
    }
}
