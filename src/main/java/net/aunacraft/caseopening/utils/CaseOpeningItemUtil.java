package net.aunacraft.caseopening.utils;

import net.aunacraft.api.bukkit.util.NBTUtils;
import net.aunacraft.caseopening.cases.CaseCache;
import net.aunacraft.caseopening.cases.CaseOpeningCase;
import net.aunacraft.caseopening.items.CaseOpeningItemExecutor;
import net.minecraft.nbt.NBTTagCompound;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class CaseOpeningItemUtil {

    public static ItemStack createCaseItem(ItemStack stack, int amount, CaseOpeningItemExecutor executor) {
        NBTTagCompound nbtTagCompound = NBTUtils.getNbtTag(stack);
        nbtTagCompound.setInt("caseopening_amount", amount);
        nbtTagCompound.setString("caseopening_executor", executor.getIdentifier());
        executor.addDataToNbtTag(nbtTagCompound);
        return NBTUtils.setNbtTag(stack, nbtTagCompound);
    }

    public static boolean isCaseItem(ItemStack stack) {
        NBTTagCompound nbtTagCompound = NBTUtils.getNbtTag(stack);
        return nbtTagCompound.hasKey("caseopening_amount");
    }

    public static CaseOpeningItemExecutor getCaseItemExecutor(ItemStack stack) {
        NBTTagCompound nbtTagCompound = NBTUtils.getNbtTag(stack);
        String executorIdentifier = nbtTagCompound.getString("caseopening_executor");
        return CaseOpeningItemExecutor.getByIdentifier(stack, executorIdentifier);
    }

    public static float getCaseItemPercentage(ItemStack stack, int totalAmount) {
        int amount = getAmount(stack);
        return (float) amount / (float) totalAmount;
    }

    public static int getAmount(ItemStack stack) {
        NBTTagCompound nbtTagCompound = NBTUtils.getNbtTag(stack);
        return nbtTagCompound.getInt("caseopening_amount");
    }

    public static ItemStack createKeyItem(UUID caseUUID, ItemStack stack) {
        NBTTagCompound nbtTagCompound = NBTUtils.getNbtTag(stack);
        nbtTagCompound.setString("caseopening_key_case_uuid", caseUUID.toString());
        return NBTUtils.setNbtTag(stack, nbtTagCompound);
    }

    public static boolean isKeyItem(ItemStack stack) {
        NBTTagCompound nbtTagCompound = NBTUtils.getNbtTag(stack);
        return nbtTagCompound.hasKey("caseopening_key_case_uuid");
    }

    public static CaseOpeningCase getCaseOfKeyItem(ItemStack stack) {
        NBTTagCompound nbtTagCompound = NBTUtils.getNbtTag(stack);
        UUID uuid = UUID.fromString(nbtTagCompound.getString("caseopening_key_case_uuid"));
        return CaseCache.getCaseByUUID(uuid);
    }
    
    public static int getTotalAmount(Inventory inventory) {
        int amount = 0;
        for (ItemStack content : inventory.getContents()) {
            if(content != null && isCaseItem(content)) {
                int contentAmount = getAmount(content);
                amount += contentAmount;
            }
        }
        return amount;
    }

}
