package net.aunacraft.caseopening.items;

import net.aunacraft.api.bukkit.util.NBTUtils;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.caseopening.cases.CaseOpeningCase;
import net.minecraft.nbt.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

public abstract class CaseOpeningItemExecutor {

    public static CaseOpeningItemExecutor giveItemExecutor = new CaseOpeningItemExecutor() {

        @Override
        public void execute(AunaPlayer player, CaseOpeningCase caseOpeningCase, ItemStack stack) {
            NBTTagCompound nbt = NBTUtils.getNbtTag(stack);
            nbt.remove("caseopening_amount");
            nbt.remove("caseopening_executor");
            ItemStack finalStack = NBTUtils.setNbtTag(stack.clone(), nbt);
            if(player.toBukkitPlayer().getInventory().firstEmpty() == -1) {
                player.toBukkitPlayer().getLocation().getWorld().dropItem(player.toBukkitPlayer().getLocation(), finalStack);
            }else {
                player.toBukkitPlayer().getInventory().addItem(finalStack);
            }
        }

        @Override
        public String getIdentifier() {
            return "give_item";
        }
    };

    public static CaseOpeningItemExecutor consoleCommandItemExecutor(String command) {
        return new CaseOpeningItemExecutor() {
            @Override
            public void execute(AunaPlayer player, CaseOpeningCase caseOpeningCase, ItemStack stack) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
                        command.replace("{player}", player.getName()).replace("%player%", player.getName()));
            }

            @Override
            public String getIdentifier() {
                return "console_command";
            }

            @Override
            public void addDataToNbtTag(NBTTagCompound nbtTag) {
                nbtTag.setString("caseopening_executor_command", command);
            }

        };
    }

    public static CaseOpeningItemExecutor playerCommandItemExecutor(String command) {
        return new CaseOpeningItemExecutor() {
            @Override
            public void execute(AunaPlayer player, CaseOpeningCase caseOpeningCase, ItemStack stack) {
                player.toBukkitPlayer().chat(command);
            }

            @Override
            public String getIdentifier() {
                return "player_command";
            }

            @Override
            public void addDataToNbtTag(NBTTagCompound nbtTag) {
                nbtTag.setString("caseopening_executor_command", command);
            }
        };
    }

    public static CaseOpeningItemExecutor getByIdentifier(ItemStack stack, String identifier) {
        if(identifier.equals("player_command")) {
            NBTTagCompound compound = NBTUtils.getNbtTag(stack);
            String command = compound.getString("caseopening_executor_command");
            return playerCommandItemExecutor(command);
        }else if(identifier.equals("console_command")) {
            NBTTagCompound compound = NBTUtils.getNbtTag(stack);
            String command = compound.getString("caseopening_executor_command");
            return consoleCommandItemExecutor(command);
        }else if(identifier.equals("give_item")){
            return giveItemExecutor;
        }
        return null;
    }

    public static boolean isValidIdentifier(String identifier) {
        return identifier.equals("player_command") || identifier.equals("console_command") || identifier.equals("give_item");
    }

    public static List<String> getIdentifiers() {
        return Arrays.asList("player_command", "console_command", "give_item");
    }


    public abstract void execute(AunaPlayer player, CaseOpeningCase caseOpeningCase, ItemStack stack);

    public abstract String getIdentifier();

    public void addDataToNbtTag(NBTTagCompound nbtTag) {}

}

