package net.aunacraft.caseopening.listener;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.caseopening.CaseOpening;
import net.aunacraft.caseopening.animation.OpeningAnimation;
import net.aunacraft.caseopening.cases.CaseOpeningCase;
import net.aunacraft.caseopening.gui.KeyViewInventory;
import net.aunacraft.caseopening.utils.CaseOpeningItemUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class InteractListener implements Listener {


    @EventHandler
    public void interact(PlayerInteractEvent event) {
        AunaPlayer player = AunaAPI.getApi().getPlayer(event.getPlayer().getUniqueId());
        if(player == null || player.isLoading()) return;
        if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block block = event.getClickedBlock();
            if(event.getHand() == EquipmentSlot.OFF_HAND) return;
            ItemStack itemInHand = event.getPlayer().getInventory().getItemInMainHand();
            if(itemInHand.getType().equals(Material.AIR)) return;
            if(!CaseOpeningItemUtil.isKeyItem(itemInHand)) return;
            CaseOpeningCase caseOpeningCase = CaseOpeningItemUtil.getCaseOfKeyItem(itemInHand);
            if(block.getLocation().equals(CaseOpening.getPlugin(CaseOpening.class).getCaseOpeningBlock().getLocation())) {
                new OpeningAnimation(caseOpeningCase, player).start();
                event.setCancelled(true);
                if(itemInHand.getAmount() > 1) {
                    itemInHand.setAmount(itemInHand.getAmount() - 1);
                }else {
                    event.getPlayer().getInventory().setItem(event.getPlayer().getInventory().getHeldItemSlot(), null);
                }
            }
        }else if(event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            if(event.getHand() == EquipmentSlot.OFF_HAND) return;
            ItemStack itemInHand = event.getPlayer().getInventory().getItemInMainHand();
            if(itemInHand.getType().equals(Material.AIR)) return;
            if(!CaseOpeningItemUtil.isKeyItem(itemInHand)) return;
            CaseOpeningCase caseOpeningCase = CaseOpeningItemUtil.getCaseOfKeyItem(itemInHand);
            new KeyViewInventory(caseOpeningCase).open(event.getPlayer());
        }
    }
}
