package net.aunacraft.caseopening.listener;

import net.aunacraft.caseopening.cases.CaseOpeningCase;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class InventoryListener implements Listener {


    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        CaseOpeningCase caseOpeningCase = CaseOpeningCase.EDITS.stream().filter(openingCase -> openingCase.isEditing() && openingCase.getEditor().equals(event.getPlayer())).findAny().orElse(null);
        if(caseOpeningCase != null) {
            caseOpeningCase.stopEditing();
            event.getPlayer().sendMessage("§aCase erfolgreich editiert!");
        }
    }
}
