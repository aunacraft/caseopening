package net.aunacraft.caseopening;

import lombok.Getter;
import lombok.SneakyThrows;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.bukkit.util.LocationUtil;
import net.aunacraft.caseopening.cases.CaseCache;
import net.aunacraft.caseopening.commands.CaseCommand;
import net.aunacraft.caseopening.database.CaseOpeningDatabaseManager;
import net.aunacraft.caseopening.listener.InteractListener;
import net.aunacraft.caseopening.listener.InventoryListener;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.plugin.java.JavaPlugin;

import javax.xml.stream.Location;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;

@Getter
public class CaseOpening extends JavaPlugin {

    private CaseOpeningDatabaseManager databaseManager;
    @Getter
    private Block caseOpeningBlock;

    @Override
    @SneakyThrows
    public void onEnable() {
        databaseManager = new CaseOpeningDatabaseManager();
        CaseCache.load();
        AunaAPI.getApi().registerCommand(new CaseCommand());

        File caseOpeningBlockLocationFile = new File(this.getDataFolder(), "blockLoc.txt");
        if(caseOpeningBlockLocationFile.exists())
            this.caseOpeningBlock = LocationUtil.locationFromString(new String(Files.readAllBytes(caseOpeningBlockLocationFile.toPath()))).getBlock();

        Bukkit.getPluginManager().registerEvents(new InventoryListener(), this);
        Bukkit.getPluginManager().registerEvents(new InteractListener(), this);
    }

    @SneakyThrows
    public void setCaseOpeningBlock(Block caseOpeningBlock) {
        this.caseOpeningBlock = caseOpeningBlock;
        File caseOpeningBlockLocationFile = new File(this.getDataFolder(), "blockLoc.txt");
        if(!caseOpeningBlockLocationFile.exists())
            caseOpeningBlockLocationFile.createNewFile();
        FileWriter writer = new FileWriter(caseOpeningBlockLocationFile);
        writer.write(LocationUtil.locationToString(this.caseOpeningBlock.getLocation()));
        writer.close();
    }
}
