package net.aunacraft.caseopening.animation;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.caseopening.CaseOpening;
import net.aunacraft.caseopening.cases.CaseOpeningCase;
import net.aunacraft.caseopening.items.CaseOpeningItemExecutor;
import net.aunacraft.caseopening.utils.CaseOpeningItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.*;

public class OpeningAnimation {

    private static final Random RANDOM = new Random();

    private final CaseOpeningCase caseOpeningCase;
    private final AunaPlayer player;
    private final ArrayList<ItemStack> items = Lists.newArrayList();
    private final int rounds;
    private final ItemStack winnerItem;
    private final CaseOpeningItemExecutor itemExecutor;
    private int currentRound = 0;
    private final OpeningGui gui = new OpeningGui();
    private final HashMap<ItemStack, Double> percentages = Maps.newHashMap();

    public OpeningAnimation(CaseOpeningCase caseOpeningCase, AunaPlayer player) {
        this.caseOpeningCase = caseOpeningCase;
        this.player = player;
        this.rounds = 40 + RANDOM.nextInt(20);
        this.initItems();
        this.winnerItem = this.items.get(rounds + 3);
        itemExecutor = CaseOpeningItemUtil.getCaseItemExecutor(winnerItem);
    }

    public void start() {
        gui.open(this.player.toBukkitPlayer());
        AunaAPI.getExecutorService().execute(() -> {
            while(currentRound < rounds) {

                GuiInventory inventory = gui.inventory;
                for(int i = 0; i < 9; i++) {
                    int slot = 9 + i;
                    ItemStack stack = items.get(i + currentRound);
                    inventory.setItem(slot, stack);
                }
                player.toBukkitPlayer().playSound(player.toBukkitPlayer().getLocation(), Sound.BLOCK_WOOL_PLACE, 2, 1);
                if(currentRound + 1 < rounds) {
                    try {
                        Thread.sleep((long) (20 + currentRound * 2L));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                currentRound++;
            }
            double percentage = this.percentages.get(this.winnerItem);
            if(percentage < 3) {
                player.toBukkitPlayer().playSound(player.toBukkitPlayer().getLocation(), Sound.ENTITY_WITHER_DEATH, 2, 1);
            }else {
                player.toBukkitPlayer().playSound(player.toBukkitPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2, 1);
            }
            Bukkit.getScheduler().scheduleSyncDelayedTask(CaseOpening.getPlugin(CaseOpening.class), () -> {
                this.itemExecutor.execute(this.player, this.caseOpeningCase, this.winnerItem);
                Bukkit.getScheduler().runTaskLater(CaseOpening.getPlugin(CaseOpening.class), () -> {
                    if(this.gui.open) {
                        player.toBukkitPlayer().closeInventory();
                        player.toBukkitPlayer().playSound(player.toBukkitPlayer().getLocation(), Sound.ENTITY_CHICKEN_EGG, 2, 1);
                    }
                }, 20 * 3);
            });
        });
    }

    private void initItems() {
        Inventory inventory = caseOpeningCase.getContentInventory();
        int totalAmount = CaseOpeningItemUtil.getTotalAmount(inventory);
        for (ItemStack content : inventory.getContents()) {
            if(content != null && CaseOpeningItemUtil.isCaseItem(content)) {
                ItemStack stack = content.clone();
                ItemMeta meta = stack.getItemMeta();
                List<String> lore = meta.hasLore() ? meta.getLore() :  Lists.newArrayList();
                double percentage = CaseOpeningItemUtil.getCaseItemPercentage(stack, totalAmount) * 100;
                DecimalFormat df = new DecimalFormat("###.##");
                lore.add(player.getMessage("caseopening.gui.item_chance",
                        df.format(percentage)));
                meta.setLore(lore);
                stack.setItemMeta(meta);
                percentages.put(stack, percentage);
                int amount = CaseOpeningItemUtil.getAmount(stack);
                for(int i = 0; i < amount; i++) {
                    items.add(stack);
                }
            }
        }
        List<ItemStack> itemList = Lists.newArrayList(this.items);
        while(items.size() + 9 < rounds) {
            items.addAll(itemList);
        }
        Collections.shuffle(this.items);
    }

    private class OpeningGui extends Gui {

        private boolean open = true;

        public OpeningGui() {
            super("§bCase Opening", 9*3);
        }

        @Override
        public void initInventory(Player player, GuiInventory guiInventory) {
            guiInventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c ").build());
            guiInventory.setItem(4, new ItemBuilder(Material.HOPPER)
                    .setDisplayName(AunaAPI.getApi().getMessageService().getMessageForPlayer(player, "caseopening.gui.your_win"))
                    .build());
        }

        @Override
        public void onClose(Player player, InventoryCloseEvent event) {
            super.onClose(player, event);
            this.open = false;
        }
    }

}
