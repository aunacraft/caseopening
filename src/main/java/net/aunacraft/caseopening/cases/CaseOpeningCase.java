package net.aunacraft.caseopening.cases;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.caseopening.CaseOpening;
import net.aunacraft.caseopening.utils.BukkitSerialization;
import net.aunacraft.caseopening.utils.CaseOpeningItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.UUID;

@Getter
public class CaseOpeningCase {

    public static final ArrayList<CaseOpeningCase> EDITS = Lists.newArrayList();

    private final UUID uuid;
    @Setter private String name;
    private final Inventory contentInventory;
    private Player editor;

    public CaseOpeningCase(UUID uuid, String name) {
        this.uuid = uuid;
        this.name = name;
        this.contentInventory = Bukkit.createInventory(null, 54, "§c" + name + " Case");
    }

    @SneakyThrows
    public CaseOpeningCase(UUID uuid, String name, String inventoryData) {
        this.uuid = uuid;
        this.name = name;
        this.contentInventory = BukkitSerialization.fromBase64(inventoryData);
    }

    public ItemStack getKeyItem() {
        return CaseOpeningItemUtil.createKeyItem(this.uuid,
                new ItemBuilder(Material.TRIPWIRE_HOOK).setDisplayName("§b" + this.name + " §3Crate").build());
    }

    public String inventoryToString() {
        return BukkitSerialization.toBase64(contentInventory);
    }

    public void update() {
        CaseOpening.getPlugin(CaseOpening.class).getDatabaseManager().updateCase(this);
    }

    public boolean isEditing() {
        return this.editor != null;
    }

    public void edit(Player player) {
        this.editor = player;
        EDITS.add(this);
        player.openInventory(contentInventory);
    }

    public void stopEditing() {
        this.editor = null;
        this.update();
    }

}
