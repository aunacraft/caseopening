package net.aunacraft.caseopening.cases;

import lombok.Getter;
import net.aunacraft.caseopening.CaseOpening;

import java.util.ArrayList;
import java.util.UUID;

public class CaseCache {

    @Getter
    private static ArrayList<CaseOpeningCase> cases;

    public static void load() {
        cases = CaseOpening.getPlugin(CaseOpening.class).getDatabaseManager().loadCases();
    }

    public static void addCase(CaseOpeningCase caseOpeningCase) {
        cases.add(caseOpeningCase);
        CaseOpening.getPlugin(CaseOpening.class).getDatabaseManager().insertCase(caseOpeningCase);
    }

    public static boolean exists(String name) {
        return cases.stream().filter(caseOpeningCase -> caseOpeningCase.getName().equalsIgnoreCase(name)).findAny().orElse(null) != null;
    }

    public static void remove(CaseOpeningCase caseOpeningCase) {
        cases.remove(caseOpeningCase);
        CaseOpening.getPlugin(CaseOpening.class).getDatabaseManager().deleteCase(caseOpeningCase);
    }

    public static CaseOpeningCase getCaseByName(String name) {
        return cases.stream().filter(chestOpeningCase -> chestOpeningCase.getName().equalsIgnoreCase(name)).findAny().orElse(null);
    }

    public static CaseOpeningCase getCaseByUUID(UUID uuid) {
        return cases.stream().filter(chestOpeningCase -> chestOpeningCase.getUuid().equals(uuid)).findAny().orElse(null);
    }

    public static void reload() {
        cases.clear();
        cases = CaseOpening.getPlugin(CaseOpening.class).getDatabaseManager().loadCases();
    }

}
