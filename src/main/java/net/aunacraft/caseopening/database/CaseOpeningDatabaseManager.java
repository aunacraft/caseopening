package net.aunacraft.caseopening.database;

import com.google.common.collect.Lists;
import lombok.SneakyThrows;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.api.database.config.impl.SpigotDatabaseConfig;
import net.aunacraft.caseopening.cases.CaseOpeningCase;
import net.aunacraft.caseopening.CaseOpening;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.UUID;

public class CaseOpeningDatabaseManager {

    private final DatabaseHandler handler;

    public CaseOpeningDatabaseManager() {
        this.handler = new DatabaseHandler(new SpigotDatabaseConfig(CaseOpening.getPlugin(CaseOpening.class)));
        createTables();
    }

    private void createTables() {
        handler.createBuilder("CREATE TABLE IF NOT EXISTS caseopening_cases(uuid VARCHAR(100) PRIMARY KEY, name VARCHAR(100) NOT NULL, inventoryData TEXT NOT NULL)").updateSync();
    }

    public void insertCase(CaseOpeningCase caseOpeningCase) {
        handler.createBuilder("INSERT INTO caseopening_cases (uuid, name, inventoryData) VALUES (?, ?, ?)")
                .addObjects(caseOpeningCase.getUuid().toString(), caseOpeningCase.getName(), caseOpeningCase.inventoryToString()).updateAsync();
    }

    public void deleteCase(CaseOpeningCase caseOpeningCase) {
        handler.createBuilder("DELETE FROM caseopening_cases WHERE uuid=?").addObjects(caseOpeningCase.getUuid().toString()).updateSync();
    }

    public void updateCase(CaseOpeningCase caseOpeningCase) {
        handler.createBuilder("UPDATE caseopening_cases SET name=?, inventoryData=? WHERE uuid=?")
                .addObjects(caseOpeningCase.getName(), caseOpeningCase.inventoryToString(), caseOpeningCase.getUuid().toString()).updateAsync();
    }

    @SneakyThrows
    public ArrayList<CaseOpeningCase> loadCases() {
        ResultSet rs = this.handler.createBuilder("SELECT * FROM caseopening_cases").querySync();
        ArrayList<CaseOpeningCase> cases = Lists.newArrayList();
        while (rs.next()) {
            cases.add(new CaseOpeningCase(
                    UUID.fromString(rs.getString("uuid")),
                    rs.getString("name"),
                    rs.getString("inventoryData")
                    ));
        }
        return cases;
    }
}
