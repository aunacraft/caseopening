package net.aunacraft.caseopening.gui;

import com.google.common.collect.Lists;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.caseopening.cases.CaseOpeningCase;
import net.aunacraft.caseopening.utils.CaseOpeningItemUtil;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.List;

public class KeyViewInventory extends Gui {

    private final CaseOpeningCase caseOpeningCase;

    public KeyViewInventory(CaseOpeningCase caseOpeningCase) {
        super(caseOpeningCase.getName(), 54);
        this.caseOpeningCase = caseOpeningCase;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        int totalAmount = CaseOpeningItemUtil.getTotalAmount(caseOpeningCase.getContentInventory());
        for (ItemStack content : caseOpeningCase.getContentInventory().getContents()) {
            if(content != null && CaseOpeningItemUtil.isCaseItem(content)) {
                ItemStack stack = content.clone();
                ItemMeta meta = stack.getItemMeta();
                List<String> lore = meta.hasLore() ? meta.getLore() :  Lists.newArrayList();
                double percentage = CaseOpeningItemUtil.getCaseItemPercentage(stack, totalAmount) * 100;
                DecimalFormat df = new DecimalFormat("###.##");
                lore.add(AunaAPI.getApi().getMessageService().getMessageForPlayer(player, "caseopening.gui.item_chance",
                        df.format(percentage)));
                meta.setLore(lore);
                stack.setItemMeta(meta);
                guiInventory.addItem(stack);
            }
        }
    }
}
