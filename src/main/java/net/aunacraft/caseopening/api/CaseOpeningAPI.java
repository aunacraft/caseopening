package net.aunacraft.caseopening.api;

import net.aunacraft.caseopening.cases.CaseCache;
import net.aunacraft.caseopening.cases.CaseOpeningCase;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CaseOpeningAPI {

    public static CaseOpeningCase getCase(String caseName) {
        return CaseCache.getCaseByName(caseName);
    }

    public static ItemStack getKeyItemStack(String caseName) {
        return getCase(caseName).getKeyItem();
    }

    public static void giveKeyItemStack(Player player, String caseName) {
        ItemStack stack = getKeyItemStack(caseName);
        if(player.getInventory().firstEmpty() == -1) {
            player.getLocation().getWorld().dropItem(player.getLocation(), stack);
        }else {
            player.getInventory().addItem(stack);
        }
    }
}
